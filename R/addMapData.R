#' @name addMapData
#' @param shp - SpatialPolygonsDataFrame that we have tidied using broom
#' @param map.df - the tidied dataframe
#' @param shp.col - string of the variable from shp to transfer across
#' @description Adds data from SpatialPolygonsDataFrame@data part into ggplot-friendly data.frame after using broom::tidy
#' @export
addMapData <- function(shp, map.df, shp.col) {

  # Unfortunately, tidy() lost the variable names ("region", in this example). Instead, we got a new variable "id", starting at 0.
  # Fortunately, the ordering of "id" is the same as that stored in shape@data$region. Let us use this to recover the names.

  # Recover row name
  temp_df <- data.frame(shp@data[shp.col])
  names(temp_df) <- c(shp.col)
  # Create and append "id"
  temp_df$id <- seq(0,nrow(temp_df)-1)
  # Merge row names with new dataframe using "id" (join is in plyr)
  df.map <- join(df.map, temp_df, by="id")

  return(df.map)
}

# In geom_map, data=name of the dataframe with data, and map the data.frame after broom has
#    tidied it up
# region - chr
# lat, long - numeric
# group - numeric
# order - integer
